//
//  User.swift
//  quiz
//
//  Created by Hoeng Linghor on 12/9/20.
//

import Foundation
import UIKit

struct AllData:Codable {
    var data: [User]
}
struct User: Codable {
    
//    var id: String?
    var first_name: String
    var last_name: String
    var avatar: String
    var email: String
    
//    enum CodingKeys: String, CodingKey {
//        case firstname = "first_name"
//        case lastname = "last_name"
//    }
}
