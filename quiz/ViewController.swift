//
//  ViewController.swift
//  quiz
//
//  Created by Hoeng Linghor on 12/9/20.
//

import UIKit

class ViewController: UIViewController {
    
    var user = [User]()
    
    @IBOutlet weak var tbView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let session = URLSession.shared
        let url = URL(string: "https://reqres.in/api/users?page=2")!
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) { data, response, error in
            if error != nil || data == nil {
                print("error!")
                return
            }
            do {
                let json = try JSONDecoder().decode(AllData.self, from: data!)
                var users = [User]()
                for data in json.data{
                    print(data)
                    users.append(data)
                }
                //print(users[0].avatar)
                self.user = users

                //self.user = users
                //print(json)
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
    }
    
}
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        user.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
        let url = URL(string: "\(user[indexPath.row].avatar)")
        let data = try? Data(contentsOf: url!)
        let avatar = UIImage(data: data!)
        cell.imageView?.image = avatar
        cell.textLabel?.text = ("\(user[indexPath.row].first_name)  \(user[indexPath.row].last_name)")
        cell.detailTextLabel?.text = user[indexPath.row].email
        return cell
    }
    
    
}


